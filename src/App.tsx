import './App.css';
import Activity from './activity/Activity';

function App() {
  return (
    <div className="App">
     <Activity/>
    </div>
  );
}

export default App;
