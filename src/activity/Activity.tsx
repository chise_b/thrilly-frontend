import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardHeader from '@mui/material/CardHeader';
import CssBaseline from '@mui/material/CssBaseline';
import Grid from '@mui/material/Grid';
import StarIcon from '@mui/icons-material/StarBorder';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';
import GlobalStyles from '@mui/material/GlobalStyles';
import Container from '@mui/material/Container';
import {useEffect, useState} from "react";
import api from '../api/API'

function Copyright(props: any) {
  return (
    <Typography variant="body2" color="text.secondary" align="center" {...props}>
      {'Copyright © '}
      <Link color="inherit" href="https://configcat.com/">
        ConfigCat
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

function ActivitiesContent() {

  const [loadingActivities, setLoadingActivities] = useState<boolean>(true);
  const [text, setText] = useState("We are searching for the best activities for you...")
  const [activities, setActivities] = useState<any[]>([]);

  const move = (arr: any[], fromIndex: number, toIndex: number) => {
    let element = arr[fromIndex];
    arr.splice(fromIndex, 1);
    arr.splice(toIndex, 0, element);
  }

  const checkForRecommended = (activities: any[]) => {
    let recommendedActivityIndex = activities.findIndex(activity => activity.recommended === true)
    if (recommendedActivityIndex !== -1){
      //we make sure that the recommended activity is in center
      move(activities, recommendedActivityIndex, 1);
    }
  }

  useEffect(() =>{
    const numberOfActivities = 3
    api.get('/activities/' + numberOfActivities).then(res =>{
      if (res.status === 200){
        checkForRecommended(res.data)
        setActivities(res.data)
      }
      else{
        setText("Ooops... Something went wrong! I think you need more inspiration...")
      }
      setLoadingActivities(false)
    })
    .catch(err =>{
      setLoadingActivities(false)
      setText("Ooops... Something went wrong! I think you need more inspiration...")
    })
  }, [])

  const handleInspiration = () =>{
    if (!loadingActivities){
      setLoadingActivities(true)
      const numberOfActivities = 3
      api.get('/activities/' + numberOfActivities).then(res =>{
        if (res.status === 200){
          checkForRecommended(res.data)
          setActivities(res.data)
        }
        else{
          setText("Ooops... Something went wrong! Please try again later...")
        }
        setLoadingActivities(false)
      })
         .catch(err =>{
           setLoadingActivities(false)
           setText("Ooops... Something went wrong! Please try again later...")
         })
    }
  }

  return (
    <React.Fragment>
      <GlobalStyles styles={{ul: { margin: 0, padding: 0, listStyle: 'none' } }} />
      <CssBaseline />
      <AppBar
        position="static"
        elevation={0}
        sx={{ backgroundColor: '#AE431E', borderBottom: (theme) => `1px solid ${theme.palette.divider}` }}
      >
        <Toolbar sx={{ color: 'white', flexWrap: 'wrap' }}>
          <Typography variant="h5" color="inherit" noWrap sx={{ flexGrow: 1 }}>
          Your daily dose of adrenaline!
          </Typography>
        </Toolbar>
      </AppBar>
      {/* Hero unit */}
      <Container disableGutters component="main" sx={{ pt: 10, pb: 8 }}>
        <Typography
          component="h1"
          variant="h2"
          align="center"
          color="white"
        >
          Thrilly
        </Typography>
        <Typography variant="h5" align="center" color="white" component="p" sx={{fontFamily: "Lucida Console", pb: 2}}>
        Do you feel bored? <br/> Let's find you something awesome to do!
        </Typography>
        <Button onClick={handleInspiration} disabled={loadingActivities}
            sx={{fontFamily: "Lucida Console", backgroundColor: '#ff3333', color: 'white',  maxWidth: 'sm', ':hover': {backgroundColor: 'red'} }}
        >
          I need inspiration!
        </Button>
      </Container>
      {/* End hero unit */}
      {(loadingActivities || (activities.length === 0 && !loadingActivities )) &&
          <Container maxWidth="md" component="main" sx={{pt: 10}}>
            <Typography
                component="h1"
                variant="h2"
                align="center"
                color="white"
            >
              {text}
            </Typography>
          </Container>
      }
      {
        !loadingActivities &&
        <Container maxWidth="md" component="main" sx={{pt: 6}}>
          <Grid container spacing={5} alignItems="flex-end">
            {activities.map((activity) => (
                // Enterprise card is full width at sm breakpoint
                <Grid
                    item
                    key={activity.type}
                    xs={10}
                    sm={activity.recommended ? 12 : 6}
                    md={4}
                >
                  <Card>
                    <CardHeader
                        title={activity.type}
                        subheader={activity.recommended ? 'Recommended by us!' : null}
                        titleTypographyProps={{ align: 'center' }}
                        action={activity.recommended ? <StarIcon style={{color: "#fcb84f"}}/> : null}
                        subheaderTypographyProps={{
                          align: 'center',
                        }}
                        sx={{
                          backgroundColor: '#f9ecd9',
                        }}
                    />
                    <CardContent>
                      <Box
                          sx={{
                            minHeight: '120px',
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'baseline',
                            pt: 6,
                            mb: 6,
                          }}
                      >
                        <Typography component="h5" variant="h6" color="#79797b">
                          {activity.activity}
                        </Typography>
                      </Box>
                    </CardContent>
                  </Card>
                </Grid>
            ))}
          </Grid>
        </Container>
      }
      {/* Footer */}
      <Container
        maxWidth="md"
        component="footer"
        sx={{
          borderTop: (theme) => `1px solid ${theme.palette.divider}`,
          mt: 8,
          pt: 8,
          pb: 8,
          py: [3, 6],
        }}
      >
        <Copyright sx={{ mt: 4 }} />
      </Container>
      {/* End footer */}
    </React.Fragment>
  );
}

export default function Activity() {
  return <ActivitiesContent />;
}